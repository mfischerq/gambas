# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 07:08+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: comp/src/gb.report2/.project:20
msgid "ReportsEvolution"
msgstr "Эволюция отчётов"

#: comp/src/gb.report2/.src/Report.class:120 comp/src/gb.report2/.src/Report.class:368
msgid "Section "
msgstr "Раздел "

#: comp/src/gb.report2/.src/Viewer/FPreview.class:212
msgid "PDF files"
msgstr "Файлы PDF"

#: comp/src/gb.report2/.src/Viewer/FPreview.class:212
msgid "Postscript files"
msgstr "Файлы Postscript"

#: comp/src/gb.report2/.src/Viewer/FPreview.class:227 comp/src/gb.report2/.src/Viewer/FPreview.class:317
msgid "Cancel"
msgstr "Отмена"

#: comp/src/gb.report2/.src/Viewer/FPreview.class:232 comp/src/gb.report2/.src/Viewer/FPreview.form:114 comp/src/gb.report2/.src/Viewer/FPreview.form:346
msgid "Print"
msgstr "Печать"

#: comp/src/gb.report2/.src/Viewer/FPreview.class:317
msgid ""
"This file already exists.\n"
"Do you want to replace it?"
msgstr ""
"Этот файл уже существует.\n"
"Вы хотите заменить его?"

#: comp/src/gb.report2/.src/Viewer/FPreview.class:317
msgid "Replace"
msgstr "Заменить"

#: comp/src/gb.report2/.src/Viewer/FPreview.class:342
msgid "Layout..."
msgstr "Макет..."

#: comp/src/gb.report2/.src/Viewer/FPreview.class:354
msgid "Printing..."
msgstr "Печать..."

#: comp/src/gb.report2/.src/Viewer/FPreview.form:5
msgid "Report preview"
msgstr "Предпросмотр отчёта"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:10
msgid "Print to file"
msgstr "Печать в файл"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:34
msgid "Printing"
msgstr "Печать"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:59
msgid "One page"
msgstr "Одна страница"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:67
msgid "Two pages"
msgstr "Две страницы"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:75
msgid "Full width"
msgstr "Полная ширина"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:83
msgid "Real size"
msgstr "Реальный размер"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:90
msgid "Zoom out"
msgstr "Уменьшить"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:101
msgid "100 %"
msgstr "100%"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:106
msgid "Zoom in"
msgstr "Увеличить"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:121
msgid "Show options"
msgstr "Показать параметры"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:141
msgid "Printer"
msgstr "Принтер"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:161
msgid "Two-sided"
msgstr "Двусторонний"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:167
msgid "None"
msgstr "Нет"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:167
msgid "Short side"
msgstr "Короткая сторона"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:167
msgid "Long side"
msgstr "Длинная сторона"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:171
msgid "File"
msgstr "Файл"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:192
msgid "Resolution"
msgstr "Разрешение"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:203
msgid "dpi"
msgstr "dpi"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:222
msgid "Range"
msgstr "Диапазон"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:235
msgid "Copies"
msgstr "Копии"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:251
msgid "Orientation"
msgstr "Ориентация"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:257
msgid "Portrait"
msgstr "Книжная"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:257
msgid "Landscape"
msgstr "Альбомная"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:265
msgid "Paper"
msgstr "Бумага"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "Custom"
msgstr "Пользовательский"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "A3"
msgstr "А3"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "A4"
msgstr "A4"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "A5"
msgstr "А5"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "B5"
msgstr "B5"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "Letter"
msgstr "Лист"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "Executive"
msgstr "Экзекьютив"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:271
msgid "Legal"
msgstr "Легал"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:283
msgid "Width"
msgstr "Ширина"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:292 comp/src/gb.report2/.src/Viewer/FPreview.form:309
msgid "mm"
msgstr "мм"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:300
msgid "Height"
msgstr "Высота"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:319
msgid "Grayscale"
msgstr "Оттенки серого"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:324
msgid "Full page"
msgstr "Полная страница"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:328
msgid "Collate copies"
msgstr "Собрать по копиям"

#: comp/src/gb.report2/.src/Viewer/FPreview.form:332
msgid "Reverse order"
msgstr "Обратный порядок"

#: comp/src/gb.report2/.src/Tests/Report10.report:14 comp/src/gb.report2/.src/Tests/Report14.report:42
msgid "=index"
msgstr "=индекс"

#: comp/src/gb.report2/.src/Tests/Report10.report:23
msgid "=Index"
msgstr "=Индекс"

#: comp/src/gb.report2/.src/Tests/Report10.report:38
msgid "=\"Index = \" & index"
msgstr "=\"Индекс = \" & индекс"

#: comp/src/gb.report2/.src/Tests/Report10.report:46
msgid "=page & \" / \" & pages"
msgstr "=страница & \" / \" & страницы"

#: comp/src/gb.report2/.src/Tests/Report13.report:30
msgid "=\"GAMBAS - \" & index"
msgstr "=\"GAMBAS - \" & индекс"

#: comp/src/gb.report2/.src/Tests/Report13.report:38 comp/src/gb.report2/.src/Tests/Report13.report:45
msgid "=\"PAGE \" & Page & \" / \" & pages"
msgstr "=\"СТРАНИЦА \" & Страница & \" / \" & страницы"

#: comp/src/gb.report2/.src/Tests/Report14.report:17
msgid "Reference"
msgstr "Ссылка"

#: comp/src/gb.report2/.src/Tests/Report14.report:24
msgid "Description"
msgstr "Описание"

#: comp/src/gb.report2/.src/Tests/Report14.report:31
msgid "Valeur"
msgstr "Значение"

#: comp/src/gb.report2/.src/Tests/Report14.report:62
msgid "=page & \" on \" & pages"
msgstr "=страница & \" на \" & страницы"

#: comp/src/gb.report2/.src/Tests/Report16.report:17 comp/src/gb.report2/.src/Tests/Report17.report:19 comp/src/gb.report2/.src/Tests/old/Report5.report:16
msgid "Coucou"
msgstr "Кукушка"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:26
msgid "Version 1.0"
msgstr "Версия 1.0"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:33 comp/src/gb.report2/.src/Tests/old/Report51.report:39
msgid "Date"
msgstr "Дата"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:48
msgid "Project Title:"
msgstr "Название проекта:"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:55
msgid "Project No.:"
msgstr "№ проекта:"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:62
msgid "Company:"
msgstr "Компания:"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:69
msgid "Designer:"
msgstr "Дизайнер:"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:83
msgid "Base Plate ID:"
msgstr "Идентификатор базовой платы:"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:134
msgid "Bearing Pressue"
msgstr "Давление подшипника"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:144 comp/src/gb.report2/.src/Tests/old/OutputReport2.report:162
msgid "Node #"
msgstr "Узел #"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:151 comp/src/gb.report2/.src/Tests/old/OutputReport2.report:169
msgid "Brg. Press., psi"
msgstr "Давление подшипника, psi"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:215
msgid "Anchor Rod Tension"
msgstr "Натяжение анкерного стержня"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:225 comp/src/gb.report2/.src/Tests/old/OutputReport2.report:243
msgid "Rod #"
msgstr "Стержень #"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:232 comp/src/gb.report2/.src/Tests/old/OutputReport2.report:250
msgid "Tension, lbs"
msgstr "Натяжение, lbs"

#: comp/src/gb.report2/.src/Tests/old/OutputReport2.report:302
msgid "Page $PAGE of $NPAGE"
msgstr "Страница $PAGE из $NPAGE"

#: comp/src/gb.report2/.src/Tests/old/Report1.report:22
msgid "=\"Page \" & Page"
msgstr "=\"Страница \" & Страница"

#: comp/src/gb.report2/.src/Tests/old/Report1.report:39
msgid "=pi() + 4"
msgstr "=пи() + 4"

#: comp/src/gb.report2/.src/Tests/old/Report11.report:7 comp/src/gb.report2/.src/Tests/old/Report12.report:7
msgid "Section 1"
msgstr "Раздел 1"

#: comp/src/gb.report2/.src/Tests/old/Report11.report:27
msgid "Gambas"
msgstr "Gambas"

#: comp/src/gb.report2/.src/Tests/old/Report11.report:47
msgid "All friends list !"
msgstr "Список всех друзей!"

#: comp/src/gb.report2/.src/Tests/old/Report11.report:69
msgid "Gambas Report Demo"
msgstr "Демонстрация отчёта Gambas"

#: comp/src/gb.report2/.src/Tests/old/Report11.report:81
msgid "DEMO"
msgstr "ДЕМО"

#: comp/src/gb.report2/.src/Tests/old/Report11.report:94
msgid "Page $PAGE on $NPAGE  "
msgstr "Страница $PAGE на $NPAGE "

#: comp/src/gb.report2/.src/Tests/old/Report2.report:16
msgid "ReportLabel1"
msgstr "Метка_отчёта_1"

#: comp/src/gb.report2/.src/Tests/old/Report3.report:15
msgid "PARCELLAIRE $NPAGE"
msgstr "ПОСЫЛКА $NPAGE"

#: comp/src/gb.report2/.src/Tests/old/Report51.report:9
msgid "Recto"
msgstr "Перёд"

#: comp/src/gb.report2/.src/Tests/old/Report51.report:17
msgid "Verso"
msgstr "Оборот"

#: comp/src/gb.report2/.src/Tests/old/Report51.report:30
msgid "N°"
msgstr "№"

#: comp/src/gb.report2/.src/Tests/old/Report51.report:78
msgid "Observations"
msgstr "Наблюдения"

#: comp/src/gb.report2/.src/Tests/old/Report6.report:10
msgid ""
"<Font bold=1><b>Test</b></font>test<br><br>\n"
"\n"
"<table border=1 cellspacing=0><tr><td><td width=20></td></td></tr></table>"
msgstr ""
"<Font bold=1><b>Тест</b></font>тест<br><br>\n"
"\n"
"<table border=1 cellspacing=0><tr><td><td width=20></td></td></tr></table>"

